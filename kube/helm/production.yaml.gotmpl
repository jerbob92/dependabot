image:
  repository: registry.gitlab.com/dependabot-gitlab/dependabot
  tag: {{ requiredEnv "CURRENT_TAG" }}

service:
  type: LoadBalancer
  port: 443
  annotations:
    service.beta.kubernetes.io/do-loadbalancer-certificate-id: {{ requiredEnv "SSL_CERT_ID" }}

projects:
  - dependabot-gitlab/dependabot
  - dependabot-gitlab/dependency-test

env:
  sentryDsn: {{ requiredEnv "DEPENDABOT_SENTRY_DSN" }}
  dependabotUrl: {{ requiredEnv "DEPENDABOT_URL" }}
  commandsPrefix: "@dependabot-bot"
  updateRetry: false
  metrics: false

credentials:
    gitlab_access_token: {{ requiredEnv "GITLAB_ACCESS_TOKEN" }}
    github_access_token: {{ requiredEnv "GITHUB_ACCESS_TOKEN" }}
    gitlab_auth_token: {{ requiredEnv "GITLAB_HOOKS_AUTH_TOKEN" }}

registriesCredentials:
  GITLAB_DOCKER_REGISTRY_TOKEN: {{ requiredEnv "GITLAB_DOCKER_REGISTRY_TOKEN" }}

redis:
  auth:
    usePassword: true
    password: {{ requiredEnv "REDIS_PASSWORD" }}

mongodb:
  auth:
    rootPassword: {{ requiredEnv "MONGODB_ROOT_PASSWORD" }}
    password: {{ requiredEnv "MONGODB_PASSWORD" }}
    username: dependabot
    database: dependabot
  strategyType: Recreate
